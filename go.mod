module gitlab.com/brymck/ted

go 1.12

require (
	github.com/mattn/go-colorable v0.1.2
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.20.0
	github.com/xanzy/go-gitlab v0.18.0
)
