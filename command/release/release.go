package release

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"github.com/xanzy/go-gitlab"
)

var Command = cli.Command{
	Name:    "release",
	Aliases: []string{"r"},
	Usage:   "release",
	Action:  action,
}

func getToken() (string, error) {
	envToken := os.Getenv("GITLAB_ACCESS_TOKEN")
	if envToken == "" {
		err := errors.New("could not retrieve API token from GITLAB_ACCESS_TOKEN environment variable")
		return "", err
	} else {
		return envToken, nil
	}
}

func findBySSHURL(projects []*gitlab.Project, sshURLToRepo string) (gitlab.Project, error) {
	for _, project := range projects {
		if project.SSHURLToRepo == sshURLToRepo {
			return *project, nil
		}
		log.Info(project)
	}
	message := fmt.Sprintf(`could not find project with SSH URL of "%s"`, sshURLToRepo)
	return gitlab.Project{}, errors.New(message)
}

func action(c *cli.Context) error {
	token, err := getToken()
	if err != nil {
		return err
	}
	git := gitlab.NewClient(nil, token)
	allProjects, _, err := git.Projects.ListUserProjects("brymck", nil)
	if err != nil {
		return err
	}
	project, err := findBySSHURL(allProjects, "git@gitlab.com:brymck/devnull.git")
	log.Info(project)
	if err != nil {
		return err
	}
	pid := strconv.Itoa(project.ID)
	log.Info(pid)
	title := "test"
	description := "description"
	options := gitlab.CreateIssueOptions{
		Title: &title,
		Description: &description,
	}
	issue, _, err := git.Issues.CreateIssue(pid, &options)
	if err != nil {
		return err
	}
	log.Info(issue)

	return nil
}
